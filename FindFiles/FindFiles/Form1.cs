﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;

namespace FindFiles
{
    public partial class Form1 : Form
    {
        //Переменный для хранения критериев поиска
        private string startDir, fileName, searchText;

        //Файл в котором будут храниться параметры прошлого поиска
        private string path = AppDomain.CurrentDomain.BaseDirectory + "param.txt";

        private Thread searching;
        private int i = 1;
        public Form1()
        {
            InitializeComponent();
            //Поток в котором будет осуществляться поиск
            searching = new Thread(FindFiles);
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Что бы убить поток поиска при выходе из формы, я убрал стандартные значки на панели и добавил свою кнопку
            this.ControlBox = false;

            //Поиск параметров с прошлого поиска
            if (File.Exists(path))
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    searchDir.Text = sr.ReadLine();
                    namePattern.Text = sr.ReadLine();
                    findText.Text = sr.ReadLine();
                }
            }

            //Делает неактивной кнопкоу паузы
            button1.Enabled = false;
        }


        //Кнопка поиска
        public void Search_Click(object sender, EventArgs e)
        {
            //Проверяет на активность поток поиска и меняется соответственно результатам
            if (!searching.IsAlive)
            {
                treeView1.Nodes.Clear();
                searching = new Thread(FindFiles);
                searching.Start();
                button1.Enabled = true;
                searchBtn.Text = "Прервать поиск";
            }
            else
            {
                searching.Abort();
                searchBtn.Text = "Начать новый поиск";
                button1.Enabled = false;
            }
            
        }


        //Кнопка паузы
        private void Button1_Click(object sender, EventArgs e)
        {
            if (i==1)
            {
                searching.Suspend();
                i = 0;
                button1.Text = "Продолжить";
            }
            else
            {
                searching.Resume();
                i = 1;
                button1.Text = "Приостановить";
            } 
            
        }

        //Метод поиска файлов и передачи данных обратно в форму
        private void FindFiles()
        {
            int counter = 1;
            string stat;
            DateTime startTime = DateTime.Now;
            TimeSpan ts;

            //Проверка на заполненность полей
            if (searchDir.Text != String.Empty)
            {
                startDir = searchDir.Text;
            }
            else
            {
                MessageBox.Show(searchDirLbl.Text);
                return;
            }
            if (namePattern.Text != String.Empty)
            {
                fileName = namePattern.Text;
            }
            else
            {
                MessageBox.Show(namePatternLbl.Text);
                return;
            }
            if (findText.Text != String.Empty)
            {
                searchText = findText.Text;
            }
            else
            {
                MessageBox.Show(findTextLbl.Text);
                return;
            }

            using (FileStream fs = File.Create(path))
            {

            }

            //Сохранение параметров поиска в файл
            using (StreamWriter sw = new StreamWriter(path))
            {
                sw.WriteLine(startDir);
                sw.WriteLine(fileName);
                sw.WriteLine(searchText);
            }

            //Поиск файлов
            string[] files = Directory.GetFiles(startDir, "*"+fileName+"*", SearchOption.AllDirectories);

            foreach (string file in files)
            {
                //Передача данных в форму
                CurrentFile(file);
                ts=DateTime.Now - startTime;
                stat = "Обработано файлов: " + counter + " За: " + ts + " секунд";
                StatInfo(stat);
                counter++;

                //Проверка на содержание фразы в файле
                try
                {
                    using (StreamReader sr = new StreamReader(file))
                    {
                        string text = sr.ReadToEnd();
                        if (text.Contains(searchText))
                        {
                            AddFileToNode(text);
                        }
                    }
                }
                catch (Exception err)
                {
                    //Если что-то пойдёт не так, записать в лог
                }
            }
        }

        
        //Делегаты для передачи данных в форму
        void CurrentFile(string file)
        {
            Action action = delegate() 
            {
                currFile.Text = file;
            };
            Invoke(action);
        }

        void StatInfo(string stat)
        {
            Action action = delegate () 
            {
                Stat.Text = stat;
            };
            Invoke(action);
        }

        //Кнопка выхода
        private void ExitBtn_Click(object sender, EventArgs e)
        {
            searching.Abort();
            this.Close();
        }

        void AddFileToNode(string file)
        {
            Action action = () => treeView1.Nodes.Add(file);
            Invoke(action);
        }

        private void TreeView1_AfterSelect(object sender, TreeViewEventArgs e)
        {

        }
    }
}
