﻿namespace FindFiles
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.searchBtn = new System.Windows.Forms.Button();
            this.searchDir = new System.Windows.Forms.TextBox();
            this.namePattern = new System.Windows.Forms.TextBox();
            this.findText = new System.Windows.Forms.TextBox();
            this.searchDirLbl = new System.Windows.Forms.Label();
            this.treeView1 = new System.Windows.Forms.TreeView();
            this.namePatternLbl = new System.Windows.Forms.Label();
            this.findTextLbl = new System.Windows.Forms.Label();
            this.currFile = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.Stat = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // searchBtn
            // 
            this.searchBtn.Location = new System.Drawing.Point(362, 260);
            this.searchBtn.Name = "searchBtn";
            this.searchBtn.Size = new System.Drawing.Size(106, 31);
            this.searchBtn.TabIndex = 0;
            this.searchBtn.Text = "Search";
            this.searchBtn.UseVisualStyleBackColor = true;
            this.searchBtn.Click += new System.EventHandler(this.Search_Click);
            // 
            // searchDir
            // 
            this.searchDir.Location = new System.Drawing.Point(420, 53);
            this.searchDir.Name = "searchDir";
            this.searchDir.Size = new System.Drawing.Size(277, 20);
            this.searchDir.TabIndex = 1;
            // 
            // namePattern
            // 
            this.namePattern.Location = new System.Drawing.Point(525, 95);
            this.namePattern.Name = "namePattern";
            this.namePattern.Size = new System.Drawing.Size(172, 20);
            this.namePattern.TabIndex = 2;
            // 
            // findText
            // 
            this.findText.Location = new System.Drawing.Point(420, 139);
            this.findText.Name = "findText";
            this.findText.Size = new System.Drawing.Size(277, 20);
            this.findText.TabIndex = 3;
            // 
            // searchDirLbl
            // 
            this.searchDirLbl.AutoSize = true;
            this.searchDirLbl.Location = new System.Drawing.Point(264, 56);
            this.searchDirLbl.Name = "searchDirLbl";
            this.searchDirLbl.Size = new System.Drawing.Size(130, 13);
            this.searchDirLbl.TabIndex = 4;
            this.searchDirLbl.Text = "Выберите дирректорию:";
            // 
            // treeView1
            // 
            this.treeView1.Location = new System.Drawing.Point(12, 53);
            this.treeView1.Name = "treeView1";
            this.treeView1.Size = new System.Drawing.Size(200, 238);
            this.treeView1.TabIndex = 5;
            this.treeView1.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.TreeView1_AfterSelect);
            // 
            // namePatternLbl
            // 
            this.namePatternLbl.AutoSize = true;
            this.namePatternLbl.Location = new System.Drawing.Point(264, 98);
            this.namePatternLbl.Name = "namePatternLbl";
            this.namePatternLbl.Size = new System.Drawing.Size(243, 13);
            this.namePatternLbl.TabIndex = 6;
            this.namePatternLbl.Text = "Введите имя файла (полностью или частично):";
            // 
            // findTextLbl
            // 
            this.findTextLbl.AutoSize = true;
            this.findTextLbl.Location = new System.Drawing.Point(264, 142);
            this.findTextLbl.Name = "findTextLbl";
            this.findTextLbl.Size = new System.Drawing.Size(143, 13);
            this.findTextLbl.TabIndex = 7;
            this.findTextLbl.Text = "Введите текст для поиска:";
            // 
            // currFile
            // 
            this.currFile.AutoSize = true;
            this.currFile.Location = new System.Drawing.Point(359, 194);
            this.currFile.Name = "currFile";
            this.currFile.Size = new System.Drawing.Size(84, 13);
            this.currFile.TabIndex = 8;
            this.currFile.Text = "Текущий файл:";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(554, 260);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(116, 31);
            this.button1.TabIndex = 9;
            this.button1.Text = "Приостановить";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Button1_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(13, 13);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(87, 23);
            this.exitBtn.TabIndex = 10;
            this.exitBtn.Text = "Выход";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.ExitBtn_Click);
            // 
            // Stat
            // 
            this.Stat.AutoSize = true;
            this.Stat.Location = new System.Drawing.Point(362, 225);
            this.Stat.Name = "Stat";
            this.Stat.Size = new System.Drawing.Size(68, 13);
            this.Stat.TabIndex = 11;
            this.Stat.Text = "Статистика:";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Stat);
            this.Controls.Add(this.exitBtn);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.currFile);
            this.Controls.Add(this.findTextLbl);
            this.Controls.Add(this.namePatternLbl);
            this.Controls.Add(this.treeView1);
            this.Controls.Add(this.searchDirLbl);
            this.Controls.Add(this.findText);
            this.Controls.Add(this.namePattern);
            this.Controls.Add(this.searchDir);
            this.Controls.Add(this.searchBtn);
            this.Name = "Form1";
            this.Text = "Поиск Файлов";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button searchBtn;
        private System.Windows.Forms.TextBox searchDir;
        private System.Windows.Forms.TextBox namePattern;
        private System.Windows.Forms.TextBox findText;
        private System.Windows.Forms.Label searchDirLbl;
        private System.Windows.Forms.TreeView treeView1;
        private System.Windows.Forms.Label namePatternLbl;
        private System.Windows.Forms.Label findTextLbl;
        private System.Windows.Forms.Label currFile;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.Label Stat;
    }
}

